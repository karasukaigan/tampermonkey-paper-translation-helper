// ==UserScript==
// @name         论文翻译助手
// @namespace    http://tampermonkey.net/
// @version      2.0.0
// @description  简单易用的论文翻译辅助工具。
// @author       鸦无量
// @include      file://*
// @include      *.pdf
// @require      https://apps.bdimg.com/libs/jquery/2.1.4/jquery.min.js
// @require      https://gitee.com/karasukaigan/tampermonkey-paper-translation-helper/raw/master/waitForKeyElements.js
// @grant        none
// ==/UserScript==
/* globals jQuery, $, waitForKeyElements */

(function () {
    'use strict';

    $("body").css("background-color", "rgb(82,86,89)");

    // 添加翻译助手到页面顶部
    $("body").prepend(
        '<div style="height: 20px; text-align: right; font-size: 14px; -webkit-user-select: none; user-select: none;">'
        + '<a id="deepl" href="https://fanyi.youdao.com/" style="text-decoration: none; color: #fff; font-size: 14px;" target="_blank">有道翻译</a> | '
        + '<a id="deepl" href="https://www.deepl.com/zh/translator" style="text-decoration: none; color: #fff; font-size: 14px;" target="_blank">DeepL</a> | '
        + '<a id="google-translate" href="https://translate.google.com.hk/" style="text-decoration: none; color: #fff; font-size: 14px;" target="_blank">Google翻译</a>'
        + '&nbsp;&nbsp;&nbsp;&nbsp;'
        + '<span id="fold-tool" style="color: #fff; font-size: 14px; cursor:pointer;">折叠</span>'
        + '&nbsp;&nbsp;&nbsp;&nbsp;'
        + '</div>'
        + '<div id="text-box" style="height: 120px; min-width: 30px;">'
        + '<textarea id="old-text" style="resize: none; height: calc(100% - 10px); width: calc(50% - 10px); border: 1px solid rgb(50,54,57); padding: 4px;">原文本</textarea>'
        + '<textarea id="new-text" style="resize: none; height: calc(100% - 10px); width: calc(50% - 10px); border: 1px solid rgb(50,54,57); padding: 4px;">规范化后的文本</textarea>'
        + '</div>'
        + '<div style="position: fixed; top: 0px; left: 0px; height: 20px; width: calc(50% + 60px); font-size: 10px;">'
        + '<button id="normal-button" style="cursor:pointer; margin-left: 10px; padding-top: 0px; padding-bottom: 2px; vertical-align: top; height: 20px; width: 52px; border: 0px; background-color: rgba(0,0,0,0); color: #fff; font-size: 12px;">规范化</button>'
        + '<button id="clear-button" style="cursor:pointer; margin-left: 10px; padding-top: 0px; padding-bottom: 2px; vertical-align: top; height: 20px; width: 52px; border: 0px; background-color: rgba(0,0,0,0); color: #fff; font-size: 12px;">清空</button>'
        + '<button id="copy-button" style="cursor:pointer; margin-left: calc(100% - 180px); padding-top: 0px; padding-bottom: 2px; vertical-align: top; height: 20px; width: 52px; border: 0px; background-color: rgba(0,0,0,0); color: #646464; font-size: 12px;">复制</button>'
        + '</div>'
    );

    // 删除默认文本
    $("#old-text").focus(function () {
        $("#old-text").text("");
    });

    // 复制文本到剪切板
    function copyText2Clipboard() {
        let text = document.getElementById("new-text");
        text.focus();
        text.select();
        document.execCommand('copy');
        text.blur();
    }

    // 规范化
    var normalButton = document.getElementById('normal-button');
    if (normalButton) {
        normalButton.addEventListener('click', function () {
            // 规范化文本
            let oldStr = $("#old-text").val();
            oldStr = oldStr.replace(/\n|\r/g, " ");
            oldStr = oldStr.replace(/\[[0-9]*\]/ig, " ");
            oldStr = oldStr.replace(/\[ [0-9]* \]/ig, " ");
            oldStr = oldStr.replace(/\+e/ig, "The");
            $("#new-text").val(oldStr);
 
            // 复制文本
            copyText2Clipboard();
        });
    }

    // 清空文本
    var clearButton = document.getElementById('clear-button');
    if (clearButton) {
        clearButton.addEventListener('click', function () {
            $("#old-text").val("");
        });
    }

    // 复制文本
    var copyButton = document.getElementById('copy-button');
    if (copyButton) {
        copyButton.addEventListener('click', function () {
            copyText2Clipboard();
        });
    }

    // 调整<embed>的位置
    waitForKeyElements("embed", function () {
        $("embed").css({"position":"relative", "height":"calc(100vh - 140px)"});
    });

    // 折叠（收起）翻译助手
    var foldTool = document.getElementById('fold-tool');
    if (foldTool) {
        foldTool.addEventListener('click', function () {
            if ($("#text-box").css("height") == "120px") {
                $("#text-box").css("height","2px");
                $("embed").css("height", "calc(100vh - 22px)");
                $("#fold-tool").text("展开");
            } else {
                $("#text-box").css("height","120px");
                $("embed").css("height", "calc(100vh - 140px)");
                $("#fold-tool").text("折叠");
            }
        });
    }
})();